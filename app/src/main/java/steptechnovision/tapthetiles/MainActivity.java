package steptechnovision.tapthetiles;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity implements onItemClick {
    private CountDownTimer countdowntimer;
    private TextView txt_score;
    private ProgressBar Progressbar;
    private TextView ShowText;
    private int progressBarValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAds();

        Progressbar = findViewById(R.id.progressBar1);
        ShowText = findViewById(R.id.textView1);
        txt_score = findViewById(R.id.txt_score);
        TextView txtBestScore = findViewById(R.id.txtBestScore);

        showFragment(Utils.boxArray[Utils.currentBoxPosition]);
        txt_score.setText(getString(R.string.score_zero));
        txtBestScore.setText(String.format(getString(R.string.best_score), PrefConnect.readInteger(MainActivity.this, PrefConnect.HIGH_SCORE, 0)));
        Utils.runningScore = 0;
        Utils.currentBoxPosition = 0;
        Utils.isStarted = false;

    }

    private void showFragment(int position) {
        GridFragment gridFragment = GridFragment.newInstance(position);
        FragmentTransaction transact = getSupportFragmentManager().beginTransaction();
        transact.add(R.id.fragment_container, gridFragment, getString(R.string.tag_gridFragment));
        transact.commit();
    }

    @Override
    public void onItemClicked() {
        changeFragment();
    }

    private void changeFragment() {
        if (Utils.currentBoxPosition + 1 >= Utils.boxArray.length) {
            Utils.runningScore++;
            countdowntimer.cancel();
            progressBarValue = 0;
            Progressbar.setProgress(progressBarValue);
            ShowText.setText(String.format(getString(R.string.txt_progress), progressBarValue, Progressbar.getMax()));
            Intent intent = new Intent(MainActivity.this, Result.class);
            startActivity(intent);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_gridFragment));
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
            Utils.runningScore++;
            txt_score.setText(String.format(getString(R.string.txt_score), Utils.runningScore));
            showFragment(Utils.boxArray[Utils.currentBoxPosition + 1]);
        }
    }

    @Override
    public void onGameStarted() {
        Utils.isStarted = true;
        int timeInterval = 1000;
        int totalTime = 60000;
        countdowntimer = new CountDownTimer(totalTime, timeInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                progressBarValue++;
                Progressbar.setProgress(progressBarValue);
                ShowText.setText(String.format(getString(R.string.txt_progress), progressBarValue, Progressbar.getMax()));
            }

            @Override
            public void onFinish() {
                progressBarValue = 0;
                Progressbar.setProgress(progressBarValue);
                ShowText.setText(String.format(getString(R.string.txt_progress), progressBarValue, Progressbar.getMax()));
                Intent intent = new Intent(MainActivity.this, Result.class);
                startActivity(intent);
            }
        };
        countdowntimer.start();
    }

    @Override
    public void onBackPressed() {
        if (countdowntimer != null) {
            countdowntimer.onFinish();
        }
        progressBarValue = 0;
        Progressbar.setProgress(progressBarValue);
        ShowText.setText(String.format(getString(R.string.txt_progress), progressBarValue, Progressbar.getMax()));
        Intent i = new Intent(MainActivity.this, Result.class);
        startActivity(i);
    }

    private void initAds() {
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(getString(R.string.admob_ad_test_device_id)).build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

}
