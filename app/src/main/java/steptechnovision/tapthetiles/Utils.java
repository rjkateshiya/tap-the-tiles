package steptechnovision.tapthetiles;

import android.content.res.TypedArray;
import android.graphics.Color;

import java.util.Random;

public class Utils {
    static int currentBoxPosition = 0;
    static int runningScore = 0;
    static boolean isStarted = false;
    static int[] boxArray = App.getAppContext().getResources().getIntArray(R.array.sample_box_array);

    static int getRandomMaterialColor() {
        TypedArray colors = App.getAppContext().getResources().obtainTypedArray(R.array.contact_colors);
        int index = (int) (Math.random() * colors.length());
        int color = colors.getColor(index, Color.BLACK);
        colors.recycle();
        return color;
    }

    static int getRndomPosition(int totalData) {
        final int min = 0;
        return new Random().nextInt((totalData - min)) + min;
    }

}