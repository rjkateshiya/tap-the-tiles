package steptechnovision.tapthetiles;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class CustomAdapter extends BaseAdapter {
    private int totalItems, colors, rightPosition;
    private LayoutInflater inflater;

    CustomAdapter(Activity activity, int totalItems, int colors, int rightPosition) {
        this.totalItems = totalItems;
        this.colors = colors;
        this.rightPosition = rightPosition;
        inflater = (LayoutInflater.from(activity));
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.grid_item, null);
        ImageView icon = view.findViewById(R.id.icon);
        icon.setBackgroundColor(colors);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            icon.setClipToOutline(true);
        }
        if (i == rightPosition) {
            icon.setAlpha(0.88f);
        }

        return view;
    }

    @Override
    public int getCount() {
        return totalItems;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}