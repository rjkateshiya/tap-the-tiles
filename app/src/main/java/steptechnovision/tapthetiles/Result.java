package steptechnovision.tapthetiles;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class Result extends AppCompatActivity {
    String action = "";
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        TextView txt_score = findViewById(R.id.txt_score);
        TextView txt_high_score = findViewById(R.id.txt_high_score);
        ImageView iv_home = findViewById(R.id.iv_home);
        ImageView iv_restart = findViewById(R.id.iv_restart);
        txt_score.setText(String.format("%s", String.valueOf(Utils.runningScore)));

        initAds();

        int highScore = PrefConnect.readInteger(Result.this, PrefConnect.HIGH_SCORE, 0);

        if (Utils.runningScore > highScore) {
            PrefConnect.writeInteger(Result.this, PrefConnect.HIGH_SCORE, Utils.runningScore);
        }

        txt_high_score.setText(PrefConnect.readInteger(Result.this, PrefConnect.HIGH_SCORE, 0) + "");

        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterstitialAd.isLoaded()) {
                    action = "home";
                    mInterstitialAd.show();
                } else {
                    Intent i = new Intent(Result.this, HomeScreen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
        });

        iv_restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterstitialAd.isLoaded()) {
                    action = "restart";
                    mInterstitialAd.show();
                } else {
                    startActivity(new Intent(Result.this, MainActivity.class));
                }
            }
        });

        Animation rotation = AnimationUtils.loadAnimation(Result.this, R.anim.rotate);
        rotation.setFillAfter(true);
        iv_restart.startAnimation(rotation);

    }

    @Override
    public void onBackPressed() {
    }

    private void initAds() {
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(getString(R.string.admob_ad_test_device_id)).build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_interstitial_unit_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.admob_ad_test_device_id)).build());

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                if (action.equals("home")) {
                    Intent i = new Intent(Result.this, HomeScreen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else {
                    startActivity(new Intent(Result.this, MainActivity.class));
                }
            }
        });
    }
}
