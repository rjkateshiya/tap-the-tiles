package steptechnovision.tapthetiles;

public interface onItemClick {
    void onItemClicked();

    void onGameStarted();
}
