package steptechnovision.tapthetiles;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class PrefConnect {
    static final String HIGH_SCORE = "high_score";
    private static final String PREF_NAME = "MY_PREF";
    private static final int MODE = Context.MODE_PRIVATE;

    static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    private static Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
