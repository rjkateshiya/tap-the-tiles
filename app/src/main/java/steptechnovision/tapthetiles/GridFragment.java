package steptechnovision.tapthetiles;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

/**
 * A simple {@link Fragment} subclass.
 */
public class GridFragment extends Fragment {
    private onItemClick onItemClick;
    private int totalBoxes = 0;

    public GridFragment() {

    }

    public static GridFragment newInstance(int position) {
        GridFragment fragment = new GridFragment();
        fragment.totalBoxes = position;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onItemClick) {
            onItemClick = (onItemClick) context;
        } else {
            throw new ClassCastException(context.toString()
                    + context.getString(R.string.error_interface));
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid, container, false);

        GridView gridView = view.findViewById(R.id.simpleGridView);
        gridView.setNumColumns(totalBoxes);
        int totalItemBox = totalBoxes * totalBoxes;
        final int rightPosition = Utils.getRndomPosition(totalItemBox);

        CustomAdapter customAdapter = new CustomAdapter(getActivity(), totalItemBox, Utils.getRandomMaterialColor(), rightPosition);
        gridView.setAdapter(customAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == rightPosition) {
                    Utils.currentBoxPosition = Utils.currentBoxPosition + 1;
                    onItemClick.onItemClicked();
                }
                if (!Utils.isStarted){
                    onItemClick.onGameStarted();
                }
            }
        });
        return view;
    }

}